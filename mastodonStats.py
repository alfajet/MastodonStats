#!/usr/bin/env python3
"""
Astuce pour rendre le JSON lisible: python -m json.tool outbox.json > pretty.json
"""

import json
import sys
from collections import defaultdict
import re

print("Statistiques en cours sur : ", sys.argv[1])

with open(sys.argv[1]) as json_data:
    d = json.load(json_data)
    # todo: add a check on JSON to ensure it matches mastodon json format

URL_PUBLIC_STREAM = "https://www.w3.org/ns/activitystreams#Public"

URL_PATTERN = r'https://[^/]+/users/[^/]+/followers'

pouets = d["orderedItems"]

mentions = defaultdict(int)
hashtags = defaultdict(int) 
stats = {"directs": 0, "privés": 0, "nonlistés": 0, "publics": 0, "repouets": 0}

url_re = re.compile(URL_PATTERN)

for pouet in pouets:

    if pouet["to"] == []:
        print("#MentionFail ? :) : ", pouet["id"][:-8])

    elif pouet["to"][0] == URL_PUBLIC_STREAM :
        if pouet["type"] == "Announce":
            stats["repouets"] += 1
        else:
            stats["publics"] += 1

    elif url_re.match(pouet["to"][0]):
        if pouet["cc"] == []:
            stats["privés"] += 1
        else:
            if URL_PUBLIC_STREAM in pouet["cc"][0]:
                stats["nonlistés"] += 1
            else:
                stats["privés"] += 1

    elif not "/followers" in pouet["to"][0]:
        stats["directs"] += 1

    else:
        print("y'a des oubliés: ", pouet["id"], pouet["to"])

    if "tag" in pouet["object"] and type(pouet["object"]) == dict:
        for tag in pouet["object"]["tag"]:
            if tag["type"] == "Mention":
                mentions[tag["name"]] += 1
            elif tag["type"] == "Hashtag":
                hashtags[tag["name"]] += 1

nb_msg_total = sum(stats.values())


def percentage(a, b):
    return str(round(a / b * 100, 2)) + " %"


stats_str = "\nNombre de messages : \n"
first = True
for k, v in stats.items():
    if first:
       first = False
    else:
        stats_str += "\n"
    stats_str += "\t" + k + " :\t" + str(v) + " (" + percentage(v, nb_msg_total) + ")"
print(stats_str)

total_mentions = sum(mentions.values())

print("\nPseudos mentionnés (par quantité décroissante) :")
for mention in sorted(mentions, key=mentions.get, reverse=True):
    print("\t{0} :\t{1} ({2})".format(
        mention,
        mentions[mention],
        percentage(mentions[mention], total_mentions)))

total_hashtag = sum(hashtags.values())

print("\nHashtag utilisés (par quantité décroissante) :")
for hashtag in sorted(hashtags, key=hashtags.get, reverse=True):
    print("\t{0} :\t{1} ({2})".format(
        hashtag,
        hashtags[hashtag],
        percentage(hashtags[hashtag],total_hashtag)))
