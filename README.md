# MastodonStats

Script qui extrait de l'archive des pouets de Mastodon des statistiques (ex: le nombre de pouets publics/privés/etc).

Pour télécharger votre archive: `https://domainedevotreinstance.tld/settings/export`
Ensuite extrayez l'archive.

Pour utiliser cet outil:
* ouvrez un terminal dans le dossier contenant le script
* (le script doit être exécutable, modifiez les droits au besoin) 
* exécutez le avec `python3 mastodonStats.py /chemin/vers/votre/archive/mastodon/output.json` (peut être un chemin relatif)